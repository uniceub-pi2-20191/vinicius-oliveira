import React, { Component } from 'react';
import { View, Text, TextInput, Button, Alert, Image, TouchableOpacity } from 'react-native';
import firebase from 'react-native-firebase';

export default class Login extends Component {
  constructor(props) {
		super(props);

		this.state = {
			Login: '',
			Senha: '',
		};

	};

	validarLogin = async () => {

		const {Login, Senha } = this.state;
		if ((Login == '' && Senha == "") || (Login == '' || Senha == "")) {
			Alert.alert('Insira usuário e/ou senha.');

		}
		else {
			try {
				await firebase.auth().signInWithEmailAndPassword(Login,Senha);
				this.props.navigation.navigate('Tela3')
			}
            catch(err){
                Alert.alert('Usuario ou senha invalidos.');  
            }	
		}
		}

  render() {
    return (
      <View >
        <View>
        </View>
        <View >
            <TextInput
            placeholder='E-mail'
            autoCapitalize="none"
            onChangeText={Login => this.setState({ Login })}
            />


            <TextInput 
            secureTextEntry
            placeholder='Senha'
            autoCapitalize="none"
            onChangeText={Senha => this.setState({ Senha })}
            />
                
            <Button style={{ margin: 20 }} 
            title = "Login"
            onPress={this.validarLogin}>

            </Button>
        </View>

        <TouchableOpacity onPress={() => this.props.navigation.navigate('Tela2')} >
							<Text> Criar conta gratuita </Text>
						</TouchableOpacity>
      </View>
    );
  }
}

