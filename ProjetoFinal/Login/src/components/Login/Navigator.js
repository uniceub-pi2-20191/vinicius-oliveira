import React, {Component} from 'react';
import {createStackNavigator,createAppContainer} from 'react-navigation';

import Login from './Login';
import Cadastro from './Cadastro';
import tela3 from './tela3'


const MainNavigator = createStackNavigator({

  Tela1: {screen: Login},
  Tela2: {screen: Cadastro},
  Tela3: {screen: tela3}

 
},
{
  initialRouteName:'Tela1'
});

const AppContainer =  createAppContainer(MainNavigator);

export default class App extends Component{
  render() {
    return <AppContainer/>;
      
  }
}
