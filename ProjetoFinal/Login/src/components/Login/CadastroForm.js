import React, { Component } from 'react';
import { View, StyleSheet, Button, TextInput, Text,Alert } from 'react-native';
import firebase from 'react-native-firebase';

export default class CadastroForm extends Component {
  constructor(props) {
    super(props);


    this.state = {
      Login: '',
      Senha: '',

    }
  }

  Cadastrar = () => {
    let { Login, Senha } = this.state;
    firebase.auth().createUserWithEmailAndPassword(Login, Senha)
    Alert.alert('Cadastro Realizado com Sucesso');}

  

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text> Cadastro </Text>
        </View>
        <View style={styles.form}>
          <TextInput style={styles.inputContainer}
            placeholder='Coloque seu E-mail: '
            autoCapitalize="none"
            onChangeText={(Login) => this.setState({ Login })}
          />
          <TextInput style={styles.inputContainer}
            placeholder='Coloque sua Senha: '
            autoCapitalize="none"
            onChangeText={(Senha) => this.setState({ Senha })}
          />
          <Button title="Registrar"
            onPress={this.Cadastrar} />
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5EFF46',
  },
  inputContainer: {
      borderBottomColor: 'rgba(255,255,255,0.3)',
      backgroundColor: 'rgba(255,255,255,0.3)',
      borderRadius:30,
      borderBottomWidth: 1,
      width:250,
      height:45,
      marginBottom:20,
      flexDirection: 'row',
      alignItems:'center'
  },
  inputs:{
      height:45,
      marginLeft:16,
      borderBottomColor: 'rgba(255,255,255,0.3)',
      flex:1,

  },
  inputIcon:{
    width:30,
    height:30,
    marginLeft:15,
    justifyContent: 'center'
  },
  buttonContainer: {
    height:45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:20,
    width:250,
    borderRadius:30,
  },
  loginButton: {
    backgroundColor: '#060004',
  },
  loginText: {
    color: 'white',
  },
});