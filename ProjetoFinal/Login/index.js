import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';

import App from './src/components/Login/Navigator';

AppRegistry.registerComponent(appName, () => App);
